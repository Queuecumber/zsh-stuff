# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="maxcustom"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how many often would you like to wait before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git tmux)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/home/mehrlich/.local/bin:/usr/local/sbin:/usr/sbin:$HOME/code/go/bin

export BROWSER=chromium-browser

alias ll="ls -l"
alias la="ls -lA"
alias op=xdg-open

#alias psearch="apt-cache search"
#alias pinstall="sudo apt-get install"
#alias premove="sudo apt-get remove"
#alias psource="apt-file search"
#alias pupdate="sudo -- sh -c 'apt-get update ; apt-get upgrade'"

prepo() { sudo add-apt-repository $1 && sudo apt-get update }

edit() { gedit $@ & disown }

#alias man="man --html"
#manpdf() { TMPFILE=$(mktemp man-$USER.XXXXXXX --tmpdir); \man -t "$1" | ps2pdf - "$TMPFILE"; op "$TMPFILE"; bash -c "sleep 2; rm $TMPFILE" & }

#alias logoff="i3-msg exit"
#alias lock="i3lock"

#bindkey "^[[1;5C" emacs-forward-word
#bindkey "^[[1;5D" emacs-backward-word
#bindkey "\e[5C" emacs-forward-word
#bindkey "\e[5D" emacs-backward-word

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

# add this configuration to ~/.zshrc
export HISTFILE=~/.zsh_history  # ensure history file visibility
export HH_CONFIG=hicolor,rawhistory        # get more colors
#bindkey -s "\C-r" "\eqhh\n"     # bind hh to Ctrl-r (for Vi mode check doc)

export EDITOR=gedit

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
if [ /usr/bin/kubectl ]; then source <(kubectl completion zsh); fi

export GOPATH=$HOME/code/go


eval $(sed '/^\[/d' /opt/adobe/sensei/creds/artifactory_credentials.properties | sed -E -n 's/[^#]+/export &/ p')
